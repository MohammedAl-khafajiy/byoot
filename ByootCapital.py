class Calculation:

    PropertyValue=0
    InvestmentAmount=0

    def SharePriceCount(self):
        SharePrice = self.PropertyValue / 1000000
        SharePrice = round(SharePrice, 2)
        return(SharePrice)

    def INVESTMENTCOSTCount(self):
        INVESTMENTCOST = self.InvestmentAmount - (self.InvestmentAmount*0.02)
        return(INVESTMENTCOST)

    def FeesCount(self):
        FEES = self.INVESTMENTCOSTCount() * 0.02
        return(FEES)

    def TOTALINVESTMENTCount(self):
        TOTALINVESTMENT = self.FeesCount() + self.INVESTMENTCOSTCount()
        return(TOTALINVESTMENT)

    def TOTALSHARESCount(self):
        TOTALSHARES = (self.InvestmentAmount/self.SharePriceCount()) - (self.InvestmentAmount/self.SharePriceCount() * 0.02)
        TOTALSHARES = round(TOTALSHARES)
        return(TOTALSHARES)



class run:
    print('Welcome To Byoot Capital')
    print('CALCULATE YOUR POTENTIAL RETURNS')
    property = Calculation()

    while True:
     try:
      property.PropertyValue = int(input("What is the Property value >> £"))
      print('Share Price will be ', property.SharePriceCount())
      break
     except ValueError:
       print ("Oops!  That was not a valid number.  Try again...")

    while True:
     try:
      property.InvestmentAmount = int(input("What is your Investment Amount >> £"))
      break
     except ValueError:
       print ("Oops!  That was not a valid number.  Try again...")

    print('Investment breakdown')
    print('INVESTMENT COST = ', property.INVESTMENTCOSTCount())
    print('TOTAL SHARES = ', property.TOTALSHARESCount())
    print('FEES = ', property.FeesCount())
    print('TOTAL INVESTMENT = ', property.TOTALINVESTMENTCount())