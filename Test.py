class Calculation:

    PropertyValue = 1000000
    InvestmentAmount = 100

    def SharePriceCount(self):
        SharePrice = self.PropertyValue / 1000000
        SharePrice = round(SharePrice, 2)
        return(SharePrice)

    def INVESTMENTCOSTCount(self):
        INVESTMENTCOST = self.InvestmentAmount - (self.InvestmentAmount*0.02)
        return(INVESTMENTCOST)

    def FeesCount(self):
        FEES = self.INVESTMENTCOSTCount() * 0.02
        return(FEES)

    def TOTALINVESTMENTCount(self):
        TOTALINVESTMENT = self.FeesCount() + self.INVESTMENTCOSTCount()
        return(TOTALINVESTMENT)

    def TOTALSHARESCount(self):
        TOTALSHARES = (self.InvestmentAmount/self.SharePriceCount()) - (self.InvestmentAmount/self.SharePriceCount() * 0.02)
        TOTALSHARES = round(TOTALSHARES)
        return(TOTALSHARES)



import unittest
class TestCalculation(unittest.TestCase):
    p = Calculation()

    def test_SharePriceCount(self):
        self.assertEqual(self.p.SharePriceCount(), 1.0)

    def test_INVESTMENTCOSTCount(self):
        self.assertEqual(self.p.INVESTMENTCOSTCount(), 98.0)

    def test_FeesCount(self):
        self.assertEqual(self.p.FeesCount(), 1.96)

    def test_TOTALINVESTMENTCount(self):
        self.assertEqual(self.p.TOTALINVESTMENTCount(), 99.96)

    def test_TOTALSHARESCount(self):
        self.assertEqual(self.p.TOTALSHARESCount(), 98)
