from tkinter import *
master = Tk()

def SharePriceCount():
    try:
        int(InvestmentAmount.get())
        int(Propertyvalue.get())
        SharePrice = int(Propertyvalue.get()) / 1000000
        SharePrice = round(SharePrice, 2)
        Label(master, text='Share Price £').grid(row=0, column=2)
        Label(master, text=SharePrice).grid(row=0, column=3)

        INVESTMENTCOST = int(InvestmentAmount.get()) - (int(InvestmentAmount.get())*0.02)
        Label(master, text='INVESTMENT COST £').grid(row=4, column=0)
        Label(master, text=INVESTMENTCOST).grid(row=5, column=0)

        FEES = INVESTMENTCOST * 0.02
        Label(master, text='FEES £').grid(row=6, column=0)
        Label(master, text=FEES).grid(row=7, column=0)

        TOTALINVESTMENT = FEES + INVESTMENTCOST
        TOTALINVESTMENT = round(TOTALINVESTMENT, 3)
        Label(master, text='TOTAL INVESTMENT').grid(row=4, column=3)
        Label(master, text=TOTALINVESTMENT).grid(row=5, column=3)

        TOTALSHARES = (int(InvestmentAmount.get())/SharePrice) - (int(InvestmentAmount.get())/SharePrice * 0.02)
        TOTALSHARES = round(TOTALSHARES)
        Label(master, text='TOTAL SHARES').grid(row=6, column=3)
        Label(master, text=TOTALSHARES).grid(row=7, column=3)

    except ValueError:
       print ("Oops!  That was not a valid number.  Try again...")

Label(master, text="Property value £").grid(row=0)
Label(master, text="Investment Amount £").grid(row=1)
Propertyvalue = Entry(master)
InvestmentAmount = Entry(master)

Propertyvalue.grid(row=0, column=1)
InvestmentAmount.grid(row=1, column=1)


Button(master, text='CALCULATE RETURNS', command=SharePriceCount).grid(row=3, column=1, sticky=W, pady=4)

mainloop()